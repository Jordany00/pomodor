# pomodor

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start
```

## Todo 

- Automatic Timer Switching
- Refactoring
- Testing
- Alerts or notifications
- Persistant
